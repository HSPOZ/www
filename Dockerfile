FROM oven/bun:1-alpine AS base
WORKDIR /app

COPY --chown=1000:1000 package.json bun.lock ./

RUN bun install --production --frozen-lockfile ;\
    chown -R 1000:1000 .

USER 1000:1000

COPY . .
RUN bun run build

ENV HOST=0.0.0.0
ENV PORT=8080
EXPOSE 8080
CMD ["bun", "--bun", "./dist/server/entry.mjs"]
