# HS Knyfyrtel website

Welcome to the repository of the Knyfyrtel Hackerspace Poznań website! 🌍 The site is available at [hspoz.pl](https://hspoz.pl/).

## ✨ Features

- Built with [AstroJS](https://astro.build/) and [Bun](https://bun.sh) 🌟
- Generates both static and dynamic content (no JavaScript required) ⚡
- Does not use cookies or trackers 🍪🚫
- Fetches event data from [Mobilizon](https://mobilizon.pl/) 📅
- Provides an RSS feed 📰
- Optimized for SEO 🔍

## 🛠 Running Locally

To run the website locally, you will need [Node.js](https://nodejs.org/) and [Yarn](https://yarnpkg.com/).

1. Install dependencies:

   ```sh
   yarn
   ```

2. Build the project:

   ```sh
   yarn run build
   ```

3. For development mode with hot-reloading:
   ```sh
   yarn run dev
   ```

## 🐳 Running with Docker

A `Dockerfile` is provided for containerized deployment.

1. Build the Docker image:

   ```sh
   docker build -t www .
   ```

2. Run the container:
   ```sh
   docker run -p 80:80 www
   ```

The website should now be available at `http://localhost:4321` 🎉.

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                    | Action                                           |
| :------------------------- | :----------------------------------------------- |
| `yarn install`             | Installs dependencies                            |
| `yarn run dev`             | Starts local dev server at `localhost:4321`      |
| `yarn run build`           | Build your production site to `./dist/`          |
| `yarn run preview`         | Preview your build locally, before deploying     |
| `yarn run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `yarn run astro -- --help` | Get help using the Astro CLI                     |

## 📜 License

This project is open-source. Feel free to contribute! 🤝
