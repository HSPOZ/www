// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = "Knyfyrtel Hackerspace Poznań";
export const SITE_DESCRIPTION = "Budujemy hakerspejs w Poznaniu.";
export const KRS = "0001143271";
export const NIP = "7812080955";
export const REGON = "54037873700000";
export const IBAN = "PL 71 2530 0008 2022 1084 2985 0001 (NEST BANK)";
export const STATUT_URL =
  "https://codeberg.org/HSPOZ/Dokumenty/raw/branch/main/statut.pdf";
