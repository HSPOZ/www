---
title: "Newsletter #2 - stowarzyszenie, lokal, 38C3, eventy, życzenia noworoczne"
pubDate: "2024-12-31"
published: true
author: "pomidor"
---

Witajcie! Dawno nie było newslettera, więc pomyślałem, że najwyższa pora nadrobić zaległości. W ciągu ostatnich kilku miesięcy sporo się działo. Z nieformalnej grupy staliśmy się pełnoprawnym stowarzyszeniem! Nasza społeczność nieustannie się powiększa, a my ruszamy z nowymi, ekscytującymi pomysłami. Zapraszam na małe podsumowanie.

---

#### **Założyliśmy stowarzyszenie!**

Z pewnością najważniejszą nowiną 2024 roku był fakt, iż wreszcie udało nam się założyć stowarzyszenie! 24 listopada odbyło się spotkanie założycielskie na którym wybraliśmy zarząd i podjęliśmy pierwsze uchwały. Po zaledwie 16 dniach otrzymaliśmy wpis do KRS -- ku naszemu zaskoczeniu, obyło bez żadnych poprawek. Od stycznia ruszamy z rejestracją członków i zbieraniem składek członkowskich. Jeśli chcecie dołączyć do naszej społeczności -- zapraszamy!

#### **Szukamy lokalu w Poznaniu**

Wraz z nowym rokiem ruszamy z akcją poszukiwania lokalu dla naszego hakerspejsu. Chcemy znaleźć miejsce z łatwym dojazdem, możliwie blisko centrum. W najbliższych tygodniach dyskusje na ten temat będą dominowały na naszym kanale Matrix -- jeśli chcielibyście nam pomóc, koniecznie dołączcie! Śledźcie także nasze kanały w social mediach, gdzie z pewnością będziemy informować o bieżących postępach.

#### **Byliśmy na 38C3**

Pod koniec grudnia nasza ekipa odwiedziła Chaos Communication Congress (38C3) w Hamburgu -- jedno z największych corocznych wydarzeń zrzeszających społeczność hakerską. Była to niesamowita okazja, by zobaczyć na żywo wiele inspirujących prelekcji, poznać różnorodne projekty, a także zintegrować się ze społecznością polskich hakerspejsów, których przedstawiciele także byli na miejscu. Serdecznie dziękujemy wszystkim, którzy byli tam z nami, za rozmowy, pomysły i świetną atmosferę -- do zobaczenia na kolejnych wydarzeniach!

#### **Nowe projekty i wydarzenia**

Rok 2025 zapowiada się dla nas niezwykle ekscytująco! W planach mamy reaktywację jednej z dużych poznańskich konferencji poświęconych wolnemu oprogramowaniu. Dodatkowo, planujemy przeprowadzenie mniejszego eventu we współpracy z Free Software Foundation Europe (FSFE). Szczegóły tych inicjatyw zdradzimy wkrótce. Jeśli natomiast sami macie pomysły na wydarzenia lub projekty, którymi chcielibyście się podzielić -- dajcie znać.

#### **Noworoczne życzenia**

Życzymy Wam w 2025 roku jak najwięcej kreatywnych pomysłów, udanych eksperymentów i jak najmniej bugów w kodzie. Obyście zawsze znajdowali czas na rozwijanie swoich hakerskich zainteresowań i zarażali innych pasją do technologii. Dziękujemy, że byliście z nami w 2024 roku! Czekamy na kolejny rok pełen inspirujących wyzwań i wspólnych działań!

---

Newsletter przygotował:\
_[@pomidor](https://matrix.to/#/@marcus_yallow:matrix.org)_
