---
title: "Newsletter #3 - szukamy lokalu, I Love Free Software Day, 1.5% podatku, FOSDEM"
pubDate: "2025-01-30"
published: true
author: "pomidor"
---

Czołem! W nowy rok wkroczyliśmy z dużym impetem! Intensywnie działamy, by nasza społeczność miała swoje miejsce. Poszukiwania siedziby dla hakerspejsu idą pełną parą, a my nie zwalniamy tempa i równolegle przygotowujemy nowe wydarzenia, wyjazdy oraz inicjatywy, które pozwolą nam jeszcze mocniej się rozwinąć.

---

#### Trwają poszukiwania lokalu

Styczeń upłynął nam pod znakiem szukania miejscówki. Nasza ekipa odwiedziła kilka lokali na mieście. Wśród nich były piwnice, biura, bloki mieszkalne, kamienice oraz pawilony handlowe. Część ofert odrzuciliśmy z racji niewystarczających wymiarów, stanu technicznego lub lokalizacji. W tej chwili mamy na oku pewien klimatyczny budynek biurowy w centrum Poznania -- na ten moment negocjacje wciąż trwają. Obserwujcie nasze kanały, aby być na bieżąco.

#### I Love Free Software Day 2025

Już za dwa tygodnie -- **13 lutego 2025 o godzinie 18:00** -- odbędzie się event **\*I Love Free Software Day 2025**,* czyli wieczór integracyjny dla entuzjastów wolnego oprogramowania. Wydarzenie organizujemy we współpracy z *Kołem Naukowym PUTrequest\_* pod oficjalnym patronatem *Free Software Foundation Europe\*. Podczas spotkania będziecie mogli wysłuchać krótkich prelekcji na temat wolnego oprogramowania oraz zjeść darmową pizzę. Więcej szczegółów przeczytacie na [stronie wydarzenia](https://hspoz.pl/ilovefs2025/).

#### Zbieramy 1.5% podatku

Miło nam ogłosić, iż w tym roku możecie przekazać nam 1.5% podatku w ramach wspierania organizacji pożytku publicznego. W tym calu należy w formularzu PIT-37 wpisać numer **KRS *0000270261*** oraz cel szczegółowy **_Knyfyrtel 25633_**. Zbiórkę prowadzi dla nas [Fundacja Studencka Młodzi Młodym](https://fsmm.pl/partner/knyfyrtel-hackerspace-poznan/). Oprócz tego przyjmujemy darowizny w postaci przelewów na nasze konto bankowe. Więcej szczegółów znajdziecie na naszej [stronie wiki](https://wiki.knyfyrtel.pl/wiki/page/finanse "Finanse").

#### Jedziemy na FOSDEM

Już za kilka dni nasza ekipa pojedzie na [FOSDEM 2025](https://fosdem.org/2025/), czyli największą konferencję poświęconą tematyce wolnego oprogramowania. Wydarzenie zaplanowane jest na 1-2 lutego 2025. Jeśli również się tam wybieracie i chcielibyście nas poznać, koniecznie dajcie znać na naszym kanale Matrix. W sobotę -- 1 lutego -- będziecie mogli nas znaleźć na [Bytenight 2025](https://bytenight.brussels/), czyli nieoficjalnym afterparty w brukselskim hakerspejsie.

---

Newsletter przygotował: \
[@pomidor](https://matrix.to/#/@marcus_yallow:matrix.org)
