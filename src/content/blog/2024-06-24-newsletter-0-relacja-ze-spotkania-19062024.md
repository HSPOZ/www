---
title: "Newsletter #0 - Relacja ze spotkania 19.06.2024"
pubDate: "2024-06-19"
published: true
author: "pomidor"
---

Cześć! To jest pierwsza wiadomość z cyklu hakerspejsowego newslettera. Na razie tylko testujemy czy lista mailingowa działa. Jeśli wszystko pójdzie dobrze, to postaramy się regularnie informować Was o postępach w budowie poznańskiego hakerspejsu. Poniżej pierwsza relacja z dzisiejszego spotkania organizacyjnego.

---

#### Kontakt z prawnikami

Mamy już opracowany statut dla naszego stowarzyszenia. Obecnie czekamy, aż prawnicy z zaprzyjaźnionego stowarzyszenia go przejrzą. Niestety, ponieważ od dłuższego czasu nie odpowiadają na maile, postanowiliśmy do nich zadzwonić. @hakki się tym zajmie i za tydzień zda relacje, co udało się ustalić.

#### Spotkanie założycielskie

Do grona osób chętnych do bycia członkami-założycielami stowarzyszenia dołączył @JuniorJPDJ. Ustaliliśmy, że fajnie byłoby mieć okrągłe 10 osób. Także brakuje nam jednego chętnego. Po krótkiej naradzie zdecydowaliśmy też, że nie będziemy się spieszyć ze zwoływaniem spotkania założycielskiego. Zamiast tego spróbujemy najpierw zorganizować kilka luźnych spotkań w realu, aby się lepiej poznać.

#### Event na WOMB

W planach mamy organizacje nieoficjalnego eventu na WOMB (Wolny Ogród Miejski Bogdanka). Będzie to luźna integracja połączona z hakowaniem i grzebaniem w sprzęcie. Raczej bez konkretnego planu. @hakki załatwi prąd na miejscu, a @JuniorJPDJ przyniesie trochę sprzętu. Wstępny termin to 30 czerwca (niedziela).

#### Warsztaty z Git-a

Natomiast po wakacjach chcemy zorganizować warsztaty z Git-a dla studentów Politechniki Poznańskiej. Od strony organizacyjnej wspiera nas Koło Naukowe Spektrum. Organizacja wydarzenia jest w toku. Przygotowywane są plakaty i opisy do social mediów.

#### Stawiamy infrastrukturę!

Kolega @JuniorJPDJ był uprzejmy podzielić się swoją domową infrastrukturą i utworzył nam VMkę, na której będziemy hostować różne spejsowe usługi. Osoby zainteresowane już otrzymały dostęp do maszynki. Dziękujemy!

---

Newsletter przygotował:
pomidor
