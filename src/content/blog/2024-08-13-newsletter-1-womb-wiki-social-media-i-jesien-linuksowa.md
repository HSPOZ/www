---
title: "Newsletter #1 - WOMB, wiki, social-media i Jesień Linuksowa"
pubDate: "2024-08-13"
published: true
author: "pomidor"
---

Czołem! W wasze ręce oddajemy kolejne wydanie hakerspejsowego newslettera. Nasza społeczność powoli się rozrasta. W ostatnim czasie do naszej grupy dołączyło kilka nowych osób. I choć rejestracja stowarzyszenia nadal jest dopiero przed nami, to nie zwalniamy tempa i wciąż organizujemy kolejne inicjatywy. Zapraszamy do czytania!

---

#### Wakacyjne spotkania na WOMBie

Jakiś czas temu ruszyliśmy z organizacją weekendowych spotkań integracyjnych na WOMB (Wolny Ogród Miejski Bogdanka). Spotkania te mają nieformalny charakter, bez konkretnego planu -- przynosimy laptopy, wspólnie hakujemy i rozmawiamy. Tak więc jeśli chcielibyście poznać naszą ekipę, to śmiało wbijajcie. Najbliższe terminy sprawdzicie w serwisach [Mobilizon](https://mobilizon.pl/@hspoz_group) oraz [Meetup](https://www.meetup.com/hspoz-pl/).

#### Strona Wiki już dostępna!

Kilka dni temu uruchomiliśmy stronę [Knyfyrtel Hackerspace Wiki](https://wiki.knyfyrtel.pl/). Witryna działa na otwartoźródłowym oprogramowaniu Bookstack. Serwis ten będzie stanowił bazę wiedzy na temat stowarzyszenia oraz naszej infrastruktury. Na ten moment dostępnych jest tam tylko kilka sekcji, ale w przyszłości będziemy regularnie dodawać nowe treści.

#### Rozkręcamy sociale

Od teraz nasz newsletter będzie publikowany nie tylko na liście mailingowej, ale także za pośrednictwem social mediów oraz [wiki](https://wiki.knyfyrtel.pl/wiki/chapter/newsletter-archiwum "Newsletter (archiwum)"). Zachęcamy Was do polubienia i śledzenia naszych profili w serwisach: [Mastodon](https://fosstodon.org/@hspoz), [Mobilizon](https://mobilizon.pl/@hspoz_group), [Meetup](https://www.meetup.com/hspoz-pl/) oraz [Facebook](https://www.facebook.com/knyfyrtel), a także do udostępniania naszych treści. Nie zapomnijcie też dołączyć do naszego oficjalnego czatu na platformie [Matrix](https://app.element.io/#/room/#hspoz:hackerspace.pl)!

#### Jedziemy na Jesień Linuksową

Miło nam ogłosić, iż zostaliśmy partnerem [Jesieni Linuksowej 2024](https://jesien.org/2024/). Wydarzenie odbędzie się w Rybniku, w dniach 25 - 27 października 2024. Jeśli interesuje was tematyka Linuksa oraz świata open-source, to gorąco zachęcamy do uczestnictwa. Oczywiście my też tam będziemy. Jeśli natomiast chcielibyście wystąpić w roli prelegenta, to zachęcamy to zgłaszania się na stronie [Call for Proposals](https://jesien.org/2024/c4p/).

---

Newsletter przygotował:\
[_@pomidor_](https://matrix.to/#/@marcus_yallow:matrix.org)
